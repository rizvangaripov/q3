package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private List<Post> allPosts;

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public static void main(String[] args) {
        new MainClass().run("src/itis/socialtest/resources/PostDatabase.csv",
                "src/itis/socialtest/resources/Authors.csv");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Author> authors = new ArrayList<>();
        allPosts = new ArrayList<>();
        try {
            FileReader fileReader = new FileReader(authorsSourcePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            for (int i = 0; i < 5; i++) {
                String[] s = bufferedReader.readLine().split(", ");
                authors.add(new Author(Long.parseLong(s[0]), s[1], s[2]));
            }
            fileReader = new FileReader(postsSourcePath);
            bufferedReader = new BufferedReader(fileReader);
            for (int i = 0; i < 9; i++) {
                String[] s = bufferedReader.readLine().split(", ");
                String content = s[3];
                for (int j = 4; j < s.length; j++) {
                    content = content + ", " + s[j];
                }
                allPosts.add(new Post(s[2], content, Long.parseLong(s[0]), authors.get(Integer.parseInt(s[0]) - 1)));
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < allPosts.size(); i++) {
            char[] c = allPosts.get(i).getDate().toCharArray();
            System.out.println("date " + c[0] + c[1] + c[2] +  c[3] + c[4] + c[5] + c[6] + c[7] + c[8] + c[9] +
                    " time" +  c[11] + c[12] + c[13] + c[14] + c[15]);
            System.out.println(allPosts.get(i).getContent());
            System.out.println("likes " + allPosts.get(i).getLikesCount());
            System.out.println(allPosts.get(i).getAuthor().getNickname() +
                    " id " + allPosts.get(i).getAuthor().getId() +
                    " birthday " + allPosts.get(i).getAuthor().getBirthdayDate());
        }
        List<Post> postsByDate = analyticsService.findPostsByDate(allPosts, "17.04.2021T10:00");
        System.out.println("posts by date");
            for (int i = 0; i < postsByDate.size(); i++) {
                char[] c = postsByDate.get(i).getDate().toCharArray();
                System.out.println("date " + c[0] + c[1] + c[2] +  c[3] + c[4] + c[5] + c[6] + c[7] + c[8] + c[9] +
                        " time" +  c[11] + c[12] + c[13] + c[14] + c[15]);
                System.out.println(postsByDate.get(i).getContent());
                System.out.println("likes " + postsByDate.get(i).getLikesCount());
                System.out.println(postsByDate.get(i).getAuthor().getNickname() +
                        " id " + postsByDate.get(i).getAuthor().getId() +
                        " birthday " + postsByDate.get(i).getAuthor().getBirthdayDate());
            }
        List<Post> postsByNick = analyticsService.findAllPostsByAuthorNickname(allPosts, "Depresnyasha");
        System.out.println("posts by nick");
        for (int i = 0; i < postsByNick.size(); i++) {
            char[] c = postsByNick.get(i).getDate().toCharArray();
            System.out.println("date " + c[0] + c[1] + c[2] +  c[3] + c[4] + c[5] + c[6] + c[7] + c[8] + c[9] +
                    " time" +  c[11] + c[12] + c[13] + c[14] + c[15]);
            System.out.println(postsByNick.get(i).getContent());
            System.out.println("likes " + postsByNick.get(i).getLikesCount());
            System.out.println(postsByNick.get(i).getAuthor().getNickname() +
                    " id " + postsByNick.get(i).getAuthor().getId() +
                    " birthday " + postsByNick.get(i).getAuthor().getBirthdayDate());
        }
    }
}
